---
menu:
  main:
    weight: 4
name: KDE Eco
title: 참여하기
userbase: KDE Eco
---
## 지속 가능한 소프트웨어 개발에 참가하기

이 의제를 계속 진행할 수 있는 의욕 있는 사람들의 도움이 필요합니다. 아래 채널을 통해서 정보를 얻고 기여할 수 있습니다.

### 대화

- Matrix 대화방: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- 메일링 리스트: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- FEEP GitLab 저장소: https://invent.kde.org/cschumac/feep
- BE4FOSS GitLab 저장소: https://invent.kde.org/joseph/be4foss

## 토론

- BigBlueButton: 월간 미팅, 매월 2번째 수요일 17:00 UTC/목요일 2:00 KST(자세한 정보는 연락해 주십시오)
- 에너지 효율성 메일링 리스트: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Matrix 대화방: https://webchat.kde.org/#/room/#energy-efficiency:kde.org

## 커뮤니티 지원

- KDE Eco 포럼: https://forum.kde.org/viewforum.php?f=334

## 연락

이메일: `joseph [at] kde.org`
