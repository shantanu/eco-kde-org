---
layout: eco-label
title: 친환경 인증
---
2020년 독일 연방환경청(Umweltbundesamt)에서는 데스크톱 소프트웨어가 환경 친화 인증인 Blauer Engel 마크를 획득할 수 있는 조건을 발표했습니다. 인증 조건에는 에너지 효율성, 하드웨어의 기대 수명 증가, 사용자 자율성 확보 등이 있으며 ... 이 모든 것은 자유 오픈 소스 소프트웨어에 부합합니다.

BE4FOSS 프로젝트는 FOSS 커뮤니티의 자원 효율적인 소프트웨어가 친환경 인증을 받는 것을 돕습니다. Blauer Engel 마크 심사는 다음 세 단계로 진행됩니다. (1) 측정, (2) 분석, (3) 인증.

1. **측정**: KDAB Berlin 등 지정된 연구소에서의 측정
1. **분석**: OSCAR(Open Source Software Consumption Analysis in R) 등 통계 도구를 사용한 분석
1. **인증**: Blauer Engel 인증 조건을 만족함을 알리는 보고서 제출

친환경 인증의 이점은 다음과 같습니다:

- 높은 수준의 친환경적 소프트웨어 디자인 표준을 만족함을 인정
- 자유 소프트웨어를 대체재와 구별
- 사용자의 제품 선택 가능성 향상
- 소프트웨어의 환경 발자국 투명성 홍보
