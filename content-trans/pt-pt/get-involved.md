---
menu:
  main:
    weight: 4
name: KDE Eco
title: Envolva-se
userbase: KDE Eco
---
## Torne-se parte do movimento de 'software' sustentável

Precisamos de muita gente o mais motivada possível para levar isto em diante. Aqui estão alguns canais onde poderá obter mais informações e contribuir.

### Comunicar

- Sala do Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Lista de correio: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Repositório de GitLab do FEEP: https://invent.kde.org/cschumac/feep
- Repositório de GitLab do BE4FOSS: https://invent.kde.org/joseph/be4foss

## Discussão

- BigBlueButton : Reuniões mensais, nas 2as Quartas às 17:00 UTC (contacte-nos para mais detalhes)
- Lista de Correio da Eficiência Energética: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Sala do Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org

## Suporte da Comunidade

- Fórum do KDE Eco: https://forum.kde.org/viewforum.php?f=334

## Contacto

E-mail: `joseph [at] kde.org`
