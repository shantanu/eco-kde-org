---
layout: eco-label
title: Emblema Eco
---
Em 2020, a Umweltbundesamt ('Agência Ambiental Alemã') lançou os critérios para a obtenção de certificações ecológicas com o emblema Blauer Engel para o 'software' de computadores. As categorias de certificação incluem a eficiência energética, a extensão do tempo de vida potencial do 'hardware' e a autonomia do utilizador … as quais se encaixam perfeitamente no 'software' livre e de código-aberto.

O projecto BE4FOSS avança com a certificação ecológica de aplicações eficientes em recursos na comunidade FOSS. A obtenção do emblema Blauer Engel ocorre em 3 passos: (1) Medir, (2) Analisar e (3) Certificar.

1. **Medir** em laboratórios dedicados, como no KDAB Berlim
1. **Analisar** com ferramentas estatísticas como o OSCAR (Open source Software Consumption Analysis in R)
1. **Certificar** através da submissão do relatório de cumprimento dos critérios da Blauer Engel

Os benefícios de obter este emblema incluem:

- Reconhecimento no alcance de padrões elevados no desenho de 'software' amigo do ambiente
- Diferenciação do 'software' livre face às alternativas
- Aumento do apelo à adopção pelos consumidores
- Promoção da transparência na pegada ecológica das aplicações
