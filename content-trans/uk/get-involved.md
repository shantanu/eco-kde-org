---
menu:
  main:
    weight: 4
name: KDE Eco
title: Участь у команді
userbase: KDE Eco
---
## Станьте частиною руху стійкого програмного забезпечення

Нам потрібно якомога більше мотивованих людей для просування ідеї. Нижче наведено деякі канали, на яких ви можете отримати додаткові відомості та взяти участь у роботі.

### Спілкування

- Кімната Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Список листування: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Сховище GitLab FEEP: https://invent.kde.org/cschumac/feep
- Сховище GitLab BE4FOSS: https://invent.kde.org/joseph/be4foss

## Обговорення

- BigBlueButton: щомісячні зустрічі, друга середа, 17:00 UTC (зв'яжіться з нами, щоб дізнатися більше)
- Список листування з енергоефективності: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Кімната Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org

## Підтримка з боку спільноти

- Форум KDE Eco: https://forum.kde.org/viewforum.php?f=334

## Контакт

Ел. пошта: `joseph [at] kde.org`
