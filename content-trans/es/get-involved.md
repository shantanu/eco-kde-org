---
menu:
  main:
    weight: 4
name: KDE Eco
title: Implicarse
userbase: KDE Eco
---
## Forme parte del movimiento de software sostenible

Necesitamos el mayor número posible de personas motivadas para impulsarlo. Aquí tiene algunos canales en los que puede obtener más información y contribuir.

### Comunicarse

- Sala de Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Lista de correo: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Repositorio GitLab de FEEP: https://invent.kde.org/cschumac/feep
- Repositorio GitLab de BE4FOSS: https://invent.kde.org/joseph/be4foss

## Discutir

- BigBlueButton: Reuniones mensuales, 2.º miércoles del mes a las 17:00 UTC (contáctenos para más detalles)
- Lista de correo de Energy Efficiency: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Sala de Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org

## Soporte de la comunidad

- Foro de KDE Eco: https://forum.kde.org/viewforum.php?f=334

## Contacto

Correo electrónico: `joseph [at] kde.org`
