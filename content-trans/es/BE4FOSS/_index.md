---
layout: eco-label
title: Ecoetiqueta
---
En 2020, la Umweltbundesamt («Agencia Alemana del Medio Ambiente») publicó los criterios de adjudicación para obtener la ecocertificación con la etiqueta *Blauer Engel* para software de escritorio. Las categorías para la certificación incluyen la eficiencia energética, la prolongación de la vida útil del hardware y la autonomía del usuario, encajando todas a la perfección con el software libre de código abierto.

El proyecto BE4FOSS avanza en la ecocertificación de software eficiente en la comunidad FOSS. La obtención de la etiqueta *Blauer Engel* conlleva tres pasos: (1) Medir, (2) Analizar y (3) Certificar.

1. **Medir** en laboratorios especializados, como el de KDAB Berlín
1. **Analizar** utilizando herramientas estadísticas, como OSCAR (Open source Software Consumption Analysis in R)
1. **Certificar** mediante la presentación del informe sobre el cumplimiento de los criterios *Blauer Engel*

Ventajas de obtener la ecoetiqueta:

- Reconocimiento por alcanzar altos niveles de diseño de software respetuoso con el medio ambiente
- Diferenciar el software libre de las alternativas
- Aumentar el atractivo de la adopción para los consumidores
- Promover la transparencia de la huella ecológica del software
