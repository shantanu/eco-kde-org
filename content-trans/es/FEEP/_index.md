---
layout: energy-efficiency
menu: ''
title: Eficiencia energética
---
FEEP está desarrollando herramientas para mejorar la eficiencia energética en el desarrollo de software libre de código abierto. El diseño y la implementación del software tienen un impacto significativo en el consumo de energía de los sistemas de los que forma parte. Con las herramientas adecuadas, es posible cuantificar y reducir el consumo de energía. Esta mayor eficiencia contribuye a un uso más sostenible de la energía como uno de los recursos compartidos de nuestro planeta.
