---
layout: eco-label
title: Certificazione ecologica
---
Nel 2020 l'Umweltbundesamt ("Agenzia tedesca per l'ambiente") ha rilasciato i criteri di aggiudicazione per ottenere la certificazione ecologica Blauer Engel per il software desktop. Le categorie per la certificazione includono l'efficienza energetica, l'estensione della potenziale della vita operativa dell'hardware e l'autonomia dell'utente... tutte caratteristiche che si adattano perfettamente al software libero e open source.

Il progetto BE4FOSS promuove la certificazione ecologica per il software efficiente in termini di risorse nella comunità FOSS. L'ottenimento della certificazione Blauer Engel avviene in 3 passaggi: (1) Misura, (2) Analizza e (3) Certifica.

1. **Misura** in laboratori dedicati, come al KDAB di Berlino
1. **Analizza** utilizzando strumenti statistici come OSCAR (Open source Software Consumption Analysis in R)
1. **Certificare** inviando la relazione sul rispetto dei criteri Blauer Engel

I vantaggi derivanti dall'ottenimento del marchio di qualità ecologica includono:

- Riconoscimento del raggiungimento di standard elevati per la progettazione di software rispettosi dell'ambiente
- Differenziare il software libero dalle alternative
- Aumentare l'attrattiva dell'adozione per i consumatori
- Promuovere la trasparenza nell'impronta ecologica del software
