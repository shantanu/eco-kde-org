---
layout: energy-efficiency
menu: ''
title: Eficiència energètica
---
El FEEP està desenvolupat eines per a millorar l'eficiència energètica en el desenvolupament de programari de codi lliure i obert. El disseny i la implementació de programari té un impacte significatiu en el consum d'energia dels sistemes que en forma part. Amb les eines correctes, és possible quantificar i reduir el consum d'energia. Aquest augment d'eficiència col·labora amb un ús més sostenible de l'energia com un dels recursos compartits del nostre planeta.
