---
menu:
  main:
    weight: 4
name: KDE Eco
title: Com participar
userbase: KDE Eco
---
## Formeu part del moviment de programari sostenible

Necessitem tanta gent motivada com sigui possible per a impulsar aquest repte. Aquí hi ha diversos canals a on podeu obtenir més informació i col·laborar.

### Comunicació

- Sala de Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Llista de correu: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Repositori GitLab del FEEP: https://invent.kde.org/cschumac/feep
- Repositori GitLab del BE4FOSS: https://invent.kde.org/joseph/be4foss

## Debats

- BigBlueButton: Trobades mensuals, 2n dimecres 17:00 UTC (contacteu amb nosaltres per als detalls)
- Llista de correu d'eficiència energètica: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Sala de Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org

## Suport de la comunitat

- Fòrum de KDE Eco: https://forum.kde.org/viewforum.php?f=334

## Contact

Correu: `joseph [at] kde.org`
